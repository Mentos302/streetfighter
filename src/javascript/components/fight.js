import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  firstFighter = fightAssign(firstFighter);
  firstFighter.healthBar = document.querySelector('#left-fighter-indicator');

  secondFighter = fightAssign(secondFighter);
  secondFighter.healthBar = document.querySelector('#right-fighter-indicator');

  return new Promise((resolve) => {
    console.log(firstFighter, secondFighter);

    document.addEventListener('keydown', function (event) {
      switch (event.code) {
        case controls.PlayerOneAttack:
          attackAction(firstFighter, secondFighter);
          break;
        case controls.PlayerOneBlock:
          firstFighter.isActiveDefense = true;
          break;
        case controls.PlayerTwoAttack:
          attackAction(secondFighter, firstFighter);
          break;
        case controls.PlayerTwoBlock:
          secondFighter.isActiveDefense = true;
          break;
        case controls.PlayerOneCriticalHitCombination:
          console.log(`egor creed`);
          if (!firstFighter.criticalCallDown) {
            secondFighter.health -= getCriticalHit(firstFighter);
            secondFighter.healthBar.style.width = `${(secondFighter.health / secondFighter.startedHealth) * 100}%`;
          }
          break;
        case controls.PlayerTwoCriticalHitCombination:
          if (!secondFighter.criticalCallDown) {
            firstFighter.health -= getCriticalHit(secondFighter);
            firstFighter.healthBar.style.width = `${(firstFighter.health / firstFighter.startedHealth) * 100}%`;
          }
          break;
      }
      if (firstFighter.health < 0) {
        resolve(secondFighter);
      } else if (secondFighter.health < 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', function (event) {
      switch (event.code) {
        case controls.PlayerOneBlock:
          firstFighter.isActiveDefense = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighter.isActiveDefense = false;
          break;
      }
      if (firstFighter.health < 0) {
        resolve(secondFighter);
      } else if (secondFighter.health < 0) {
        resolve(firstFighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);

  if (damage < 0) {
    return 0;
  } else {
    return damage;
  }
}

export function getHitPower(fighter) {
  const power = fighter.attack * Math.random() * 2;

  return power;
}

export function getBlockPower(fighter) {
  const power = fighter.defense * Math.random() * 2;

  return power;
}

const attackAction = (attacker, defender) => {
  if (!attacker.isActiveDefense && !defender.isActiveDefense) {
    defender.health -= getDamage(attacker, defender);
    defender.healthBar.style.width = `${(defender.health / defender.startedHealth) * 100}%`;
  }
};

const getCriticalHit = (fighter) => {
  fighter.criticalCallDown = true;

  setTimeout(() => (fighter.criticalCallDown = false), 10000);
  return fighter.attack * 2;
};

const fightAssign = (fighter) => {
  return Object.assign(
    {
      startedHealth: fighter.health,
      criticalCallDown: false,
      isActiveDefense: false,
    },
    fighter
  );
};
