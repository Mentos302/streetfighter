import { showModal } from './modal';

export function showWinnerModal(fighter) {
  console.log(fighter);

  showModal({
    title: `Battle is over. Winner is ${fighter.name}`,
    bodyElement: `Binary academy 2021`,
    onClose: () => location.reload(),
  });
}
