import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  fighterElement.innerHTML = fighter
    ? `
  
  <h3 class="fighter-preview___name">
    ${fighter.name}
  </h3>
  <img src=${fighter ? fighter.source : null}>
  <div class="fighter-preview___metainfo">
  Health: ${fighter.health}<br>
    Attack: ${fighter.attack}
    Defense: ${fighter.defense}
  </div>`
    : null;

  console.log(fighter);
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
